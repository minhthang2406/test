//
//  ViewController2.m
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "ViewController2.h"
#import "ViewController2Model.h"
#import "TableViewCellModel.h"
#import "TableCellTableViewCell.h"
#import "ViewController3.h"
@interface ViewController2 ()
{
    ViewController2Model *_model02;

    
}

@end

@implementation ViewController2

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _model02 = [[ViewController2Model alloc]init];
        _image02.image = [[UIImage alloc]init];
        // Custom initialization
    }
    return self;
}
- (void) nextToView3
{
    ViewController3 *view3 = [[ViewController3 alloc] initWithNibName:@"ViewController3" bundle:nil];
    view3.delegate3 = self;
    [self.navigationController pushViewController:view3 animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setModel02:_model02];
       
    //UIBarButtonItem *newRightButton =
  //  [[UIBarButtonItem alloc] initWithTitle:@"Collection"
              //                       style:UIBarButtonItemStyleBordered
                //                    target:nil
                  //                  action:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFastForward target:self action:@selector(nextToView3)];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) setModel02 : (ViewController2Model*)model2
{
    _title02.text = model2.title02String;
    _image02.image = [UIImage imageNamed:model2.imageo2String];
}

#pragma Table Data Sourse
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_model02.listPlayers count];
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Creat ID for Cell
    static NSString *tableCellID = @"TableCell";
    TableCellTableViewCell *cell = [_table dequeueReusableCellWithIdentifier:tableCellID];
    if (cell == nil)
    {
        cell = [[TableCellTableViewCell alloc]init];
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TableCellTableViewCell" owner:nil options:nil]firstObject];
    }
    //lay du lieu tu mang
    NSString *tenCauthu = [_model02.listPlayers objectAtIndex:indexPath.row];
    NSString *tenFileAnh = [_model02.listImageNames objectAtIndex:indexPath.row];
    //khoi tao cellmodel
    TableViewCellModel *cellModel = [[TableViewCellModel alloc]init];
    TableViewCellModel *cellModel02 = [[TableViewCellModel alloc]init];
    cellModel.playerNameString = tenCauthu;
    cellModel02.playerImageString = tenFileAnh;
    [cell setModel:cellModel];
    [cell setModel2:cellModel02];
  //  [cell setModel:tenCauthu];
 //   [cell setModel2:tenFileAnh];
    return cell;

}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Select ROW");
    [self nextToView3];
}








@end
