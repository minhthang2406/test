//
//  ViewControllerModel.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ViewControllerModel;
@interface ViewControllerModel : NSObject
@property (nonatomic, strong) NSString *title01String;
@property (nonatomic, strong) NSString *image01String;
- (void)setModel01 : (ViewControllerModel *)model1;
@end
