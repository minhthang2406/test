//
//  TableCellTableViewCell.m
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "TableCellTableViewCell.h"
#import "TableViewCellModel.h"

@implementation TableCellTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    _playerName.text = @"tenCauThu";
    _playerImage.image = [[UIImage alloc]init];

    

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void) setModel : (TableViewCellModel*)modelCell
{
    _playerName.text = modelCell.playerNameString;
}
- (void) setModel2:(TableViewCellModel*)modelCell2
{
    _playerImage.image = [UIImage imageNamed:modelCell2.playerImageString];
}
@end
