//
//  MyCell.m
//  DanhSachBarca
//
//  Created by MAC on 6/23/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "MyCell.h"
#import "MyCellModel.h"
@implementation MyCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _playerName03.text = @"Player Name";
        _image3.image = [[UIImage alloc]init];
        // Initialization code
    }
    return self;
}
- (void) setModel : (MyCellModel*)modelCell03
{
    _playerName03.text = modelCell03.playerNames;
    _image3.image = [UIImage imageNamed:modelCell03.playerImages];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
