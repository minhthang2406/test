//
//  ViewController2.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ViewColtroller2Delegate <NSObject>
@end
@interface ViewController2 : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UILabel *title02;
@property (strong, nonatomic) IBOutlet UIImageView *image02;
@property (strong, nonatomic) IBOutlet UITableView *table;

@property (nonatomic, assign) id<ViewColtroller2Delegate>delagate2;
@end
