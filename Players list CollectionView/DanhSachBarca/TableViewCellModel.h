//
//  TableViewCellModel.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TableViewCellModel : NSObject
@property (nonatomic,strong) NSString *playerNameString;
@property (nonatomic,strong) NSString *playerImageString;


@end
