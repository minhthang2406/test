//
//  ViewController3Model.m
//  DanhSachBarca
//
//  Created by MAC on 6/21/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "ViewController3Model.h"

@implementation ViewController3Model
- (id) init
{
    self = [super init];
    {
        _danhSachTenCauThu = [[NSMutableArray alloc] initWithObjects: @"Daniel Alves",@"Bravo",@"Inesta",@"Messi",@"Millito", @"Neymar", @"Pique", @"Rafinha",@"Rakitic",@"Busquets", nil];
        
        _danhSachAnhCauThu = [[NSMutableArray alloc]initWithObjects:@"Alves.jpg",@"bravo.jpg",@"inesta.jpg",@"messi.jpg",@"millito.jpg", @"neymar.jpg", @"pique.jpg", @"rafinha.jpg",@"rakitic.jpg",@"busquets.jpg", nil];
    }
    return self;
}
@end
