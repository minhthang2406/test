//
//  TableCellTableViewCell.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TableViewCellModel;
@interface TableCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *playerName;
@property (strong, nonatomic) IBOutlet UIImageView *playerImage;
- (void) setModel : (TableViewCellModel*)modelCell;
- (void) setModel2 : (TableViewCellModel*)modelCell2;
@end
