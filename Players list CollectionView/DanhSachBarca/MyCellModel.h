//
//  MyCellModel.h
//  DanhSachBarca
//
//  Created by MAC on 6/23/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyCellModel : NSObject
@property (nonatomic, strong) NSString*playerNames;
@property (nonatomic, strong) NSString *playerImages;
@end
