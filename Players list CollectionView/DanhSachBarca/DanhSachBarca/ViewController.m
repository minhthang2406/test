//
//  ViewController.m
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "ViewController.h"
#import "ViewController2.h"
#import "ViewControllerModel.h"
@interface ViewController ()
{
    ViewControllerModel *model01;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    model01 = [[ViewControllerModel alloc]init];
    _image01.image = [[UIImage alloc]init];
    [self setModel01:model01];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setModel01 : (ViewControllerModel *)model1
{
    _title01.text = model1.title01String;
    _image01.image = [UIImage imageNamed:model1.image01String];
    
}
- (IBAction)nextToTeamList:(id)sender
{
    ViewController2 *view2 = [[ViewController2 alloc] initWithNibName:@"ViewController2" bundle:nil];
    view2.delagate2 = self;
    [self.navigationController pushViewController:view2 animated:YES];
}
@end
