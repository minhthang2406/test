//
//  ViewController.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *title01;
@property (strong, nonatomic) IBOutlet UIImageView *image01;

- (IBAction)nextToTeamList:(id)sender;

@end
