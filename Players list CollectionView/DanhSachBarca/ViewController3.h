//
//  ViewController3.h
//  DanhSachBarca
//
//  Created by MAC on 6/21/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ViewController3Delegate <NSObject>
@end
@interface ViewController3 : UIViewController
@property (nonatomic, assign) id<ViewController3Delegate> delegate3;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
