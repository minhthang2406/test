//
//  ViewController2Model.m
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "ViewController2Model.h"

@implementation ViewController2Model
- (id)init
{
    self = [super init];
    if (self)
    {
        _title02String = @"Palyer List";
        _imageo2String = @"barca.jpg";
        _listPlayers = [[NSMutableArray alloc]initWithObjects:@"Daniel Alves",@"Bravo",@"Inesta",@"Messi",@"Millito", @"Neymar", @"Pique", @"Rafinha",@"Rakitic",@"Busquets", nil];
        _listImageNames = [[NSMutableArray alloc]initWithObjects:@"Alves.jpg",@"bravo.jpg",@"inesta.jpg",@"messi.jpg",@"millito.jpg", @"neymar.jpg", @"pique.jpg", @"rafinha.jpg",@"rakitic.jpg",@"busquets.jpg", nil];
        
    }
    return self;
}
@end
