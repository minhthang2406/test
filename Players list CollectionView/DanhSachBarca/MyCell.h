//
//  MyCell.h
//  DanhSachBarca
//
//  Created by MAC on 6/23/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MyCellModel;
@interface MyCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *image3;
@property (strong, nonatomic) IBOutlet UILabel *playerName03;
- (void) setModel : (MyCellModel*)modelCell03;
//- (void) setModel2 : (MyCellModel*)modelCell02;

@end
