//
//  ViewController3.m
//  DanhSachBarca
//
//  Created by MAC on 6/21/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "ViewController3.h"
#import "ViewController3Model.h"
#import "MyCell.h"
#import "MyCellModel.h"
@interface ViewController3 ()
{
    ViewController3Model *model3;
}

@end

@implementation ViewController3

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
           model3 = [[ViewController3Model alloc]init];
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.collectionView registerNib:[UINib nibWithNibName:@"MyCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma collection Data

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [model3.danhSachTenCauThu count];
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellID = @"CELL";
    MyCell *cell = [_collectionView dequeueReusableCellWithReuseIdentifier:CellID forIndexPath:indexPath];
    if (cell == nil)
    {
        cell = [[MyCell alloc]init];
        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyCell" owner:nil options:nil]firstObject];
        
    }
    NSString *tenCauThu = [model3.danhSachTenCauThu objectAtIndex:indexPath.row];
    NSString *anhCauTHu = [model3.danhSachAnhCauThu objectAtIndex:indexPath.row];
    MyCellModel *cellModel = [[MyCellModel alloc]init];
    cellModel.playerNames = tenCauThu;
    cellModel.playerImages = anhCauTHu;
    [cell setModel:cellModel];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   // MyCell *cell = (MyCell*)[collectionView cellForItemAtIndexPath:indexPath];
  //  NSArray *views = [cell.contentView subviews];
    //UILabel *label = [views objectAtIndex:0];
    NSLog(@"Select Cell");
}



@end
