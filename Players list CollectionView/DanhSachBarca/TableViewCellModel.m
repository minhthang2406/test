//
//  TableViewCellModel.m
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import "TableViewCellModel.h"

@implementation TableViewCellModel
- (id) init
{
    self = [super init];
    if (self)
    {
        _playerImageString = @"PlayerImage";
        _playerNameString  = @"PlayerName";
    }
    return self;
}
@end
