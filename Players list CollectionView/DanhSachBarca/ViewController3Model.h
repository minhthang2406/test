//
//  ViewController3Model.h
//  DanhSachBarca
//
//  Created by MAC on 6/21/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ViewController3Model : NSObject
@property (nonatomic, strong) NSMutableArray *danhSachTenCauThu;
@property (nonatomic, strong) NSMutableArray *danhSachAnhCauThu;
@end
