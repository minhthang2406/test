//
//  ViewController2Model.h
//  DanhSachBarca
//
//  Created by MAC on 6/15/15.
//  Copyright (c) 2015 Minh Thang. All rights reserved.
//

#import <Foundation/Foundation.h>
@class ViewController2Model;
@interface ViewController2Model : NSObject
@property (nonatomic,strong) NSString *title02String;
@property (nonatomic,strong) NSString *imageo2String;
@property (strong,nonatomic) NSMutableArray *listPlayers;
@property (strong,nonatomic) NSMutableArray *listImageNames;
//@property (strong, nonatomic) NSMutableArray *listPlayerPosition;
- (void) setModel02 : (ViewController2Model*)model2;
@end
